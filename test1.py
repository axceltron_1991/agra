import urllib
import sys
import json
import requests
import os

#print (os.environ["client_id"])
base_url = 'https://login.microsoftonline.com/fcb2b37b-5da0-466b-9b83-0014b67a7c78/oauth2/v2.0/token'
client_id= os.environ["client_id"]
client_secret= os.environ["client_secret"]
grant_type= os.environ["grant_type"]
scope= os.environ["scope"]
response = requests.post(base_url,auth=(client_id, client_secret),data={'grant_type':grant_type,'client_id':client_id,'client_secret':client_secret,'scope':scope})
json_response = response.json()
print (json_response["access_token"])


