FROM ubuntu:
RUN apt-get update && apt-get install -y python3
RUN apt install python3-pip -y
RUN pip install requests
